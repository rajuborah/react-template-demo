/* eslint-disable no-unused-vars */
import { defaultOnChangeHandler } from 'components/items/FormBuilder/formBuilder.utils';
import FORM_ACTION_TYPES from 'components/items/FormBuilder/formBuilder.actionTypes';
import { _validatePattern } from 'utils/common';
import { initAuth } from './home.actions';

const ACTION_HANDLERS = {
  [FORM_ACTION_TYPES.ON_FIELD_CHANGE]: (action, stateHelpers) => {
    const { payload: { key } } = action;
    const funcToExec = defaultOnChangeHandler;
    funcToExec(action, stateHelpers);
  },
  [FORM_ACTION_TYPES.ON_FORM_SUBMIT]: async (_, { getState, setState, dispatch }) => {
    const { values } = getState();
    // TODO:: notification need to be added
    if (!values || values.password.length === 0 || values.indentifier) return;
    dispatch(initAuth(values));
  },
};
export default ACTION_HANDLERS;
