import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import FormBuilder from 'components/items/FormBuilder';
import withActions from 'utils/connectors/withActions';
import { EMPTY_OBJECT } from 'constants/app.constants';
import authControlReducer from 'reducers/authControl.reducer';
import withAsyncReducer from 'utils/connectors/withAsyncReducer';
import { getAuthInfo } from 'base/AuthControl/authControl.selectors';
import { AUTH_INFO_STORE_KEY } from 'base/AuthControl/authControl.constants';
import ACTION_HANLDERS from './home.actionHandlers';
import { getFormSections, getFormField } from './home.config';
import styles from './home.module.css';
const Home = (props) => {
    const { onAction, values } = props;
    return (
        <FormBuilder
            // className={styles.formControl}
            sections={getFormSections()}
            fields={getFormField()}
            onAction={onAction}
            values={values}
            submitButtonText="SUBMIT"
        />
    );
}

Home.propTypes = {
    onAction: PropTypes.func.isRequired,
    values: PropTypes.object,
};

Home.defaultProps = {
    values: EMPTY_OBJECT,
};

export default compose(
    withAsyncReducer({ key: AUTH_INFO_STORE_KEY, reducer: authControlReducer }),
    connect(getAuthInfo),
)(withActions(ACTION_HANLDERS)(Home));
