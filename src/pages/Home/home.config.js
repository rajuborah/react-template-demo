import TextInputFieldRenderer from 'components/pieces/FormFieldRenderers/TextInputFieldRenderer';
import { EMAIL_PATTERN, EMPTY_STRING } from 'constants/app.constants';
import { TEXT_INPUT_TYPES } from 'components/bits/TextInput/textInput.constants';
import { FORM_FIELD_KEYS } from './home.constants';
import styles from './home.module.css';
export const getFormSections = () => {
  const rows = [
    { columns: [FORM_FIELD_KEYS.IDENITIFIER] },
    { columns: [FORM_FIELD_KEYS.PASSWORD] },
  ];
  const sections = [
    { rows },
  ];
  return sections;
};

export const getFormField = () => {
  const formFields = FORM_FIELDS;
  return formFields;
};
const FORM_FIELDS = {

  [FORM_FIELD_KEYS.IDENITIFIER]: {
    renderer: TextInputFieldRenderer,
    renderOptions: {
      label: 'E-mail',
      size: "small",
      fieldClassName: styles.label,
      classes: styles.label,
      showLabels: false,
      length: [0, 64],
      lengthLabel: EMPTY_STRING,
      pattern: EMAIL_PATTERN,
      patternLabel: EMPTY_STRING,
    },
  },

  [FORM_FIELD_KEYS.PASSWORD]: {
    renderer: TextInputFieldRenderer,
    renderOptions: {
      label: 'Password',
      size: "small",
      type: TEXT_INPUT_TYPES.PASSWORD,
      fieldClassName: styles.label,
      showLabels: false,
      length: [0, 100],
      lengthLabel: EMPTY_STRING,
      pattern: /.*/,
      patternLabel: EMPTY_STRING,
    },
  },
};
