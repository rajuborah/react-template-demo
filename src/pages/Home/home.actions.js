/* eslint-disable guard-for-in */
import AUTH_CONTROL_ACTIONS from 'constants/redux-actions/authControl.actions';
import app from 'utils/axios';

export const login = (data, callback) => {
  const url = '/api/auth/local';
  const request = {
    method: 'POST',
    url,
    data: data,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    }
  };
  app.invokeApi(request, (success, res) => {
    callback(success, res);
  });
};

export const initAuth = values => (dispatch) => {
  dispatch({ type: AUTH_CONTROL_ACTIONS.AUTHENTICATION_IN_PROGRESS });
  // make api call
  login(values, (success, res) => {
    if (success) {
      console.log("res");
      const userInfo = {
        access_token: res.jwt,
        name: res.user.name,
        userEmail: res.user.email,
        userId: res.user.user_id,
      };
      dispatch({ type: AUTH_CONTROL_ACTIONS.AUTHENTICATION_SUCCESS, payload: userInfo });
    } else {
      dispatch({ type: AUTH_CONTROL_ACTIONS.AUTHENTICATION_FAILURE });
    }
  });
};