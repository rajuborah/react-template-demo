/* eslint-disable guard-for-in */
import AUTH_CONTROL_ACTIONS from 'constants/redux-actions/authControl.actions';
import app from 'utils/axios';
import { storeData } from 'utils/asyncStorage';

export const login = (data, callback) => {
  const url = '/api/auth/local';
  const request = {
    method: 'POST',
    url,
    data: data
  };
  app.invokeApi(request, (success, res) => {
    callback(success, res);
  });
};

export const initAuth = values => (dispatch) => {
  dispatch({ type: AUTH_CONTROL_ACTIONS.AUTHENTICATION_IN_PROGRESS });
  // make api call
  login(values, (success, res) => {
    if (success) {
      const userInfo = {
        access_token: res.jwt,
        username: res.user.username,
        email: res.user.email,
        userId: res.user.user_id,
        token: res,
      };
      storeData('token', JSON.stringify(res));
      dispatch({ type: AUTH_CONTROL_ACTIONS.AUTHENTICATION_SUCCESS, payload: userInfo });
    } else {
      dispatch({ type: AUTH_CONTROL_ACTIONS.AUTHENTICATION_FAILURE });
    }
  });
};