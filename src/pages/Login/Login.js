import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import FormBuilder from 'components/items/FormBuilder';
import withActions from 'utils/connectors/withActions';
import { EMPTY_OBJECT } from 'constants/app.constants';
import authControlReducer from 'reducers/authControl.reducer';
import withAsyncReducer from 'utils/connectors/withAsyncReducer';
import { getAuthInfo } from 'base/AuthControl/authControl.selectors';
import { AUTH_INFO_STORE_KEY } from 'base/AuthControl/authControl.constants';
import ACTION_HANLDERS from './login.actionHandlers';
import { getFormSections, getFormField } from './login.config';
import styles from './login.module.css';
import login from 'assets/images/login.svg'
import loginBg from 'assets/images/loginBg.jpg';
import logo from 'assets/logo/logo.png';
class LoginPage extends PureComponent {
  componentDidMount() {
    const { location } = this.props;
    const ssoError = new URLSearchParams(location.search).get('ssoError');
    if (ssoError) {
      // notify.error('SSO Error, Please try again!');
    }
  }

  render() {
    const { onAction, values, isAuthenticationInProgress } = this.props;

    return (
      <div className={styles.mainContainer} style={{ backgroundImage: `url(${loginBg})` }}>
        <div className={styles.loginContainer}>
          <div className={styles.leftSideConatiner}>
            <img src={login} alt="" style={{ width: "300px" }} />
          </div>
          <div className={styles.rightSideContainer}>
            <div className={styles.externalLoginDiv}>
              <div className={styles.logoConatiner}>
                <img src={logo} alt="XopunTech" style={{ width: '100px' }} />
              </div>
              <h1 className={styles.externalLoginHeader}>Exam Portal</h1>
              <FormBuilder
                className={styles.formControl}
                sections={getFormSections()}
                fields={getFormField()}
                onAction={onAction}
                values={values}
                submitButtonText="LOG IN"
                isSubmitLoading={isAuthenticationInProgress}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

LoginPage.propTypes = {
  onAction: PropTypes.func.isRequired,
  values: PropTypes.object,
  isAuthenticationInProgress: PropTypes.bool,
};

LoginPage.defaultProps = {
  values: EMPTY_OBJECT,
  isAuthenticationInProgress: false,
};

export default compose(
  withAsyncReducer({ key: AUTH_INFO_STORE_KEY, reducer: authControlReducer }),
  connect(getAuthInfo),
)(withActions(ACTION_HANLDERS)(LoginPage));
