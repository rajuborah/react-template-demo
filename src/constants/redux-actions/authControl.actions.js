const AUTH_CONTROL_ACTIONS = {
  AUTHENTICATION_IN_PROGRESS: 'AUTHENTICATION_IN_PROGRESS',
  AUTHENTICATION_SUCCESS: 'AUTHENTICATION_SUCCESS',
  LOGOUT_SUCCESS: 'LOGOUT_SUCCESS',
  AUTHENTICATION_FAILURE: 'AUTHENTICATION_FAILURE',
  AUTO_AUTHENTICATION_FAILURE: 'AUTO_AUTHENTICATION_FAILURE',
};
export default AUTH_CONTROL_ACTIONS;