export const EMPTY_OBJECT = {};
export const EMPTY_ARRAY = [];
export const EMPTY_STRING = '';

export const ACCEPTABLE_IMAGE_FILES = '.jpg,.png,.jpeg';

export const DEFAULT_LENGTH = [0, 40];
export const DEFAULT_LENGTH_LABEL = 'Maximum 40 characters';
export const DEFAULT_TEXT_INPUT_PATTERN = /^[\w\d\s]*$/i;
export const DEFAULT_PATTERN_LABEL = 'Only alphabets and numbers allowed';

export const EVENT_ID_LENGTH = [2, 40];
export const EVENT_ID_LENGTH_LABEL = 'Minimum 2 and Maximum 40 characters';
export const EVENT_ID_TEXT_INPUT_PATTERN = /^[a-zA-Z0-9-_]+$/i;
export const EVENT_ID_PATTERN_LABEL = 'Only alphabets, numbers, dashes and underscores are allowed';

// TITLE
export const TITLE_LENGTH = [0, 100];
export const TITLE_LENGTH_LABEL = 'Maximum 100 characters';
export const TITLE_PATTERN = /^[\w\W\s]*$/i;
export const TITLE_PATTERN_LABEL = 'Alphabets, numbers and special characters accepted';

// DESCRIPTION
export const DESCRIPTION_LENGTH = [0, 500];
export const DESCRIPTION_LENGTH_LABEL = 'Maximum 500 characters';
export const DESCRIPTION_PATTERN = /^[\w\W\s]*$/i;
export const DESCRIPTION_PATTERN_LABEL = 'Alphabets, numbers and special characters accepted';

export const EMAIL_PATTERN = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export const PASSWORD_PATTERN = /^$|^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;

// SOCKET
export const GLOBAL_BROADCAST_SESSION_ID = 'broadcast';
export const DM_BROADCAST_SESSION_ID = 'dmBroadcast';

export const AUTH_INFO_STORE_KEY = "authInfo";
export const GLOBAL_STORE_KEY = "globalInfo";

export const getGlobalInfo = (state) => state[GLOBAL_STORE_KEY];
