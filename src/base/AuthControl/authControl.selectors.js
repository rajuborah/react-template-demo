import { AUTH_INFO_STORE_KEY } from './authControl.constants';

export const getAuthInfo = state => state[AUTH_INFO_STORE_KEY];
