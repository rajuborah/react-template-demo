import AUTH_CONTROL_ACTIONS from 'constants/redux-actions/authControl.actions';
import { getData, removeData } from 'utils/asyncStorage';
export const getToken = (setState) => (dispatch) => {
  setState({ checkSessionInProgress: true });
  getData(
    'token',
    (token) => {
      var tokenObj = JSON.parse(token);
      if (
        tokenObj !== null &&
        tokenObj.expireAt &&
        new Date(tokenObj.expireAt) < new Date()
      ) {
        console.log("object");
        removeData('token');
        setState({ checkSessionInProgress: false });
        dispatch({
          type: AUTH_CONTROL_ACTIONS.AUTHENTICATION_FAILURE,
        });

        //this.getAuthenticationToken();
      } else {
        setState({ checkSessionInProgress: false });
        const userInfo = {
          access_token: tokenObj.jwt,
          username: tokenObj.user.username,
          email: tokenObj.user.email,
          userId: tokenObj.user.user_id,
          token: tokenObj,
        };
        dispatch({
          type: AUTH_CONTROL_ACTIONS.AUTHENTICATION_SUCCESS,
          payload: userInfo,
        });
      }
    },
    (err) => {
      setState({ checkSessionInProgress: false });
      dispatch({
        type: AUTH_CONTROL_ACTIONS.AUTHENTICATION_FAILURE,
      });
    }
  );
};