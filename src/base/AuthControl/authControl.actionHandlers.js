/* eslint-disable no-unused-vars */
import AUTHCONTROL_ACTION_TYPES from './authControl.actionTypes';
import { getSession, getToken } from './authControl.actions';
const ACTION_HANDLERS = {
  [AUTHCONTROL_ACTION_TYPES.ON_PAGE_LOAD]: async (_, { getState, setState, dispatch }) => {
    dispatch(getToken(setState));
  },
};

export default ACTION_HANDLERS;
