import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { EMPTY_OBJECT } from 'constants/app.constants';
import authControlReducer from 'reducers/authControl.reducer';
import withAsyncReducer from 'utils/connectors/withAsyncReducer';
import withActions from 'utils/connectors/withActions';
import Spinner from 'components/pieces/Spinner';
import ACTION_HANLDERS from './authControl.actionHandlers';
import { getAuthInfo } from './authControl.selectors';
import { AUTH_INFO_STORE_KEY } from './authControl.constants';
import AUTHCONTROL_ACTION_TYPES from './authControl.actionTypes';
import App from '../App';
import styles from './authControl.module.css';
class AuthControl extends PureComponent {
  componentDidMount() {
    const { onAction, isUserLoggedIn } = this.props;
    if (!isUserLoggedIn) {
      onAction({ type: AUTHCONTROL_ACTION_TYPES.ON_PAGE_LOAD });
    }
  }

  render() {
    const { isUserLoggedIn, checkSessionInProgress, userInfo } = this.props;
    return checkSessionInProgress ?
      <Spinner spinSize="large" containerClassName={styles.authControlContainer} /> :
      <App isUserLoggedIn={isUserLoggedIn} userInfo={userInfo} />;
  }
}

AuthControl.propTypes = {
  onAction: PropTypes.func.isRequired,
  isUserLoggedIn: PropTypes.bool,
  checkSessionInProgress: PropTypes.bool,
  userInfo: PropTypes.object,
};

AuthControl.defaultProps = {
  isUserLoggedIn: false,
  checkSessionInProgress: false,
  userInfo: EMPTY_OBJECT,
};

export default compose(
  withAsyncReducer({ key: AUTH_INFO_STORE_KEY, reducer: authControlReducer }),
  connect(getAuthInfo),
)(withActions(ACTION_HANLDERS)(AuthControl));
