/* eslint-disable react/prop-types */
import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import ResourceNotFound from 'base/ResourceNotFound';
import BaseLayout from '../../Layout/BaseLayout';

const AdminRoutes = () => (
  <Switch>
    <Redirect exact from="/" to="/home" />
    <Redirect exact from="/login" to="/home" />
    <Route exact path="/home" component={BaseLayout} />
    <Route component={ResourceNotFound} />
  </Switch>
);

export default AdminRoutes;
