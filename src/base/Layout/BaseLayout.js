import React from "react";
import NavBar from "components/pieces/NavBar";
import styles from "./baseLayout.module.css";
import Home from "../../pages/Home";
import { Route } from "react-router-dom";
// import { AUTH_INFO_STORE_KEY } from "base/AuthControl/authControl.constants";
// import authControlReducer from "reducers/authControl.reducer";
// import { getAuthInfo } from "base/AuthControl/authControl.selectors";
// import { compose } from "redux";
// import withAsyncReducer from "utils/connectors/withAsyncReducer";
// import { connect } from "react-redux";

const BaseLayout = () => {
  return (
    <div>
      <NavBar />
      <div className={styles.pageContent}>
        {/* child */}
        <Route exact path="/home" component={Home} />
      </div>
    </div>
  );
}

export default BaseLayout;
// export default compose(
//   withAsyncReducer({ key: AUTH_INFO_STORE_KEY, reducer: authControlReducer }),
//   connect(getAuthInfo)
// )(BaseLayout);
