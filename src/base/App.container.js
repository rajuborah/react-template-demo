import React from 'react';
import { Provider as ReduxProvider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import { makeStore } from 'utils/redux';

import AuthControl from './AuthControl';

// This Component is supposed to render only once. Donot add any side-effects.
function AppContainer() {
  const store = makeStore();
  return (
    <ReduxProvider store={store}>
      <BrowserRouter>
        <AuthControl />
      </BrowserRouter>
    </ReduxProvider>
  );
}

export default AppContainer;
