import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import styles from './resourceNotFound.module.css';
class ResourceNotFound extends PureComponent {
  render() {
    return (
      <>
        <div className={styles.resourceNotFoundContainer}>
          <div>404</div>
          <div>Page Not Found</div>
          <div><Link to="/" className={styles.homeLink} >Click here for Homepage</Link></div>
        </div>
      </>
    );
  }
}

export default ResourceNotFound;
