import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import Login from 'pages/Login';
import withStore from 'utils/connectors/withStore';
import AdminRoutes from 'base/Routes/AdminRoutes';

function App({ store, isUserLoggedIn, userInfo }) {

  return (
    <React.Fragment>
      {isUserLoggedIn ? (
        <React.Fragment>
          <AdminRoutes />
        </React.Fragment>
      ) : (
        <Switch>
          <Route exact path="/login" component={Login} />
          <Route exact path="*" component={Login} />
        </Switch>
      )}
    </React.Fragment>
  );
}

App.propTypes = {
  store: PropTypes.object.isRequired,
  isUserLoggedIn: PropTypes.bool.isRequired,
  userInfo: PropTypes.object.isRequired,
};

export default withStore(React.memo(App));
