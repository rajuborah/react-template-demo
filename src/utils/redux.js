import thunk from 'redux-thunk';
import {
  createStore, applyMiddleware, compose, combineReducers,
} from 'redux';

import { _identity } from './common';

export const makeStore = () => {
  // We create a store with no initial reducers, where reducers will be injected asynchronously as and when required.
  // See withAsyncReducer HOC for more details
  const store = createStore(_identity, compose(
    applyMiddleware(thunk),
    window.devToolsExtension ? window.devToolsExtension() : _identity
  ));
  store.asyncReducers = {};
  return store;
};

export const injectReducer = (key, reducer, store) => {
  const updatedAsyncReducers = { ...store.asyncReducers, [key]: reducer };
  store.asyncReducers = updatedAsyncReducers; // eslint-disable-line no-param-reassign
  store.replaceReducer(combineReducers(updatedAsyncReducers));
};
