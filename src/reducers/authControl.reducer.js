import produce from 'immer';
import AUTH_CONTROL_ACTIONS from 'constants/redux-actions/authControl.actions';
import { EMPTY_OBJECT } from 'constants/app.constants';

const DEFAULT_STATE = {
  isAuthenticationInProgress: false,
  isUserLoggedIn: false,
  autoLoginError: false,
  userInfo: EMPTY_OBJECT,
};

export default (state = DEFAULT_STATE, action) => produce(state, (draft) => {
  switch (action.type) {
    case AUTH_CONTROL_ACTIONS.AUTHENTICATION_IN_PROGRESS:
      draft.isAuthenticationInProgress = true;
      break;
    case AUTH_CONTROL_ACTIONS.AUTHENTICATION_SUCCESS:
      draft.isUserLoggedIn = true;
      draft.isAuthenticationInProgress = false;
      draft.userInfo = action.payload;
      break;
    case AUTH_CONTROL_ACTIONS.AUTO_AUTHENTICATION_FAILURE:
      draft.isUserLoggedIn = false;
      draft.isAuthenticationInProgress = false;
      draft.autoLoginError=true;
      break;
    case AUTH_CONTROL_ACTIONS.AUTHENTICATION_FAILURE:
      draft.isUserLoggedIn = false;
      draft.isAuthenticationInProgress = false;
      break;
    case AUTH_CONTROL_ACTIONS.LOGOUT_SUCCESS:
      draft.isUserLoggedIn = false;
      draft.userInfo = EMPTY_OBJECT;
      break;
    default:
  }
});
