import produce from 'immer';
import { EMPTY_ARRAY, EMPTY_OBJECT } from 'constants/app.constants';
import GLOBAL_ACTIONS from 'constants/redux-actions/global.actions';
const DEFAULT_STATE = {
  profileInfo: EMPTY_OBJECT,
};

export default (state = DEFAULT_STATE, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case GLOBAL_ACTIONS.FETCH_PROFILE_INFO:
        draft.profileInfo = action.payload;
        break;
      default:
    }
  });
