import TextArea from 'components/bits/TextArea';
import withFieldRenderer from './withFieldRenderer';

const TextAreaRenderer = withFieldRenderer(TextArea);

export default TextAreaRenderer;
