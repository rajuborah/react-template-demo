import SelectInput from 'components/bits/SelectInput';
import withFieldRenderer from './withFieldRenderer';

const SelectRenderer = withFieldRenderer(SelectInput);

export default SelectRenderer;
