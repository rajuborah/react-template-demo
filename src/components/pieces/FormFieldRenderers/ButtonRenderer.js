import ButtonInput from 'components/bits/ButtonInput';
import withFieldRenderer from './withFieldRenderer';

const ButtonRenderer = withFieldRenderer(ButtonInput);

export default ButtonRenderer;
