import WorkspaceToolbar from 'components/pieces/WorkspaceToolbar';

import { mapProps, compose } from 'recompose';

import withFieldRenderer from './withFieldRenderer';


const WorkspaceToolbarFieldRenderer = compose(
  withFieldRenderer,
  mapProps(({ value: selectedToolId, ...rest }) => ({ ...rest, selectedToolId }))
)(WorkspaceToolbar);

export default WorkspaceToolbarFieldRenderer;
