import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { EMPTY_STRING } from 'constants/app.constants';
import FORM_ACTION_TYPES from '../../items/FormBuilder/formBuilder.actionTypes';
import styles from './withFieldRenderer.module.css';

function withFieldRenderer(ComposedComponent) {
  class WithFieldRenderer extends PureComponent {
    handleChange = (value) => {
      const { onAction, id } = this.props;
      onAction({ type: FORM_ACTION_TYPES.ON_FIELD_CHANGE, payload: { value, key: id } });
      console.log("ON FIELD CHNAGED", id, value);
    }

    handleError = (value) => {
      const { onAction, id } = this.props;
      onAction({ type: FORM_ACTION_TYPES.ON_FIELD_ERROR, payload: { value, key: id } });
    }

    render() {
      const {
        onAction, labelClassName, fieldClassName, ...restProps
      } = this.props;
      return (
        <div className={`${styles.fieldContainer} ${fieldClassName}`}>
          {/* {label && <p className={[styles.label, labelClassName]}>{label}</p>} */}
          <ComposedComponent {...restProps} onChange={this.handleChange} onError={this.handleError} />
        </div>
      );
    }
  }

  WithFieldRenderer.propTypes = {
    onAction: PropTypes.func.isRequired,
    value: PropTypes.any,
    label: PropTypes.string,
    id: PropTypes.string.isRequired,
    fieldClassName: PropTypes.string,
    labelClassName: PropTypes.string,
  };

  WithFieldRenderer.defaultProps = {
    value: undefined,
    label: EMPTY_STRING,
    fieldClassName: EMPTY_STRING,
    labelClassName: EMPTY_STRING,
  };

  return WithFieldRenderer;
}

export default withFieldRenderer;
