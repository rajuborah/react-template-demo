import Checkbox from 'components/bits/Checkbox';
import withFieldRenderer from './withFieldRenderer';

const CheckboxRenderer = withFieldRenderer(Checkbox);

export default CheckboxRenderer;
