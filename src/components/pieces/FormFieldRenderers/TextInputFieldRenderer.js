import TextInput from 'components/bits/TextInput';
import withFieldRenderer from './withFieldRenderer';

const TextInputFieldRenderer = withFieldRenderer(TextInput);

export default TextInputFieldRenderer;
