import { compose, mapProps } from 'recompose';

import WorkspaceView from 'components/pieces/WorkspaceView';

import withFieldRenderer from './withFieldRenderer';

const WorkspaceViewFieldRenderer = compose(
  withFieldRenderer,
  mapProps(({ value: layoutConfig, ...rest }) => ({ ...rest, layoutConfig }))
)(WorkspaceView);

export default WorkspaceViewFieldRenderer;
