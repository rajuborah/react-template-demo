import AUTH_CONTROL_ACTIONS from "constants/redux-actions/authControl.actions";
import { removeData } from 'utils/asyncStorage';
export const logout = (getState) => (dispatch) => {
    const { history } = getState();
    removeData("token", () => {
        dispatch({ type: AUTH_CONTROL_ACTIONS.LOGOUT_SUCCESS });
        history.push("/login");
    });
};