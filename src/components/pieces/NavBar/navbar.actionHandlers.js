import ACTION_TYPES from "./navbar.actionTypes";
import { logout } from "./navbar.actions";

const ACTION_HANDLERS = {
  [ACTION_TYPES.HANDLE_LOGOUT]: async (_, { getState, dispatch }) => {
    dispatch(logout(getState));
  },
};

export default ACTION_HANDLERS;
