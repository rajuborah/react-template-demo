import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import AdbIcon from '@mui/icons-material/Adb';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { getGlobalInfo, GLOBAL_STORE_KEY } from '../../../constants/app.constants';
import withActions from "utils/connectors/withActions";
import withAsyncReducer from "utils/connectors/withAsyncReducer";
import globalReducer from "reducers/global.reducer";
import ACTION_TYPES from './navbar.actionTypes';
import ACTION_HANDLERS from './navbar.actionHandlers';
import logo from 'assets/logo/logo.png';
import { ThemeProvider, createTheme } from '@mui/material/styles';
function NavBar(props) {
    const { onAction } = props;
    const [anchorElUser, setAnchorElUser] = React.useState(null);

    const darkTheme = createTheme({
        palette: {
            mode: 'dark',
            primary: {
                main: '#464747',
            },
        },
    });

    const handleOpenUserMenu = (event) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };
    const handleLogout = () => {
        onAction({
            type: ACTION_TYPES.HANDLE_LOGOUT,
        });
    };
    return (
        <ThemeProvider theme={darkTheme}>
            <AppBar position="static" enableColorOnDark color="primary">
                <Container maxWidth="xl">
                    <Toolbar disableGutters>
                        <img src={logo} style={{ width: '50px' }} />
                        <Typography
                            variant="h6"
                            noWrap
                            component="a"
                            href="/"
                            sx={{
                                mr: 2,
                                display: { xs: 'none', md: 'flex' },
                                fontFamily: 'monospace',
                                fontWeight: 700,
                                letterSpacing: '.3rem',
                                color: 'inherit',
                                textDecoration: 'none',
                            }}
                        >
                            XopunTech
                        </Typography>

                        <Typography
                            variant="h5"
                            noWrap
                            component="a"
                            href=""
                            sx={{
                                mr: 2,
                                display: { xs: 'flex', md: 'none' },
                                flexGrow: 1,
                                fontFamily: 'monospace',
                                fontWeight: 700,
                                letterSpacing: '.3rem',
                                color: 'inherit',
                                textDecoration: 'none',
                            }}
                        >
                            XopunTech
                        </Typography>
                        <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
                        </Box>
                        <Box sx={{ flexGrow: 0 }}>
                            <Tooltip title="Open settings">
                                <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                                    <Avatar alt="Remy Sharp" />
                                </IconButton>
                            </Tooltip>
                            <Menu
                                sx={{ mt: '45px' }}
                                id="menu-appbar"
                                anchorEl={anchorElUser}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={Boolean(anchorElUser)}
                                onClose={handleCloseUserMenu}
                            >
                                <MenuItem key={'Logout'} onClick={handleLogout}>
                                    <Typography textAlign="center">Logout</Typography>
                                </MenuItem>
                            </Menu>
                        </Box>
                    </Toolbar>
                </Container>
            </AppBar>
        </ThemeProvider>
    );
}
// export default NavBar;
export default compose(
    withActions(ACTION_HANDLERS),
    withAsyncReducer({ key: GLOBAL_STORE_KEY, reducer: globalReducer }),
    connect(getGlobalInfo)
)(NavBar);