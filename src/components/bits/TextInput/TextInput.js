import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import TextField from '@mui/material/TextField';
import classNames from 'classnames';
import { _validateTextLength, _validatePattern, _noop } from 'utils/common';
import {
  DEFAULT_LENGTH,
  DEFAULT_LENGTH_LABEL,
  DEFAULT_TEXT_INPUT_PATTERN,
  DEFAULT_PATTERN_LABEL,
} from 'constants/app.constants';
import styles from './textInput.module.css';

class TextInput extends PureComponent {
  state = { error: false }

  handleChange = (event) => {
    const { onChange } = this.props;
    this.setState({ error: false });
    onChange(event.target.value, event);
  };

  handleError = (event, value) => {
    const { onError } = this.props;
    onError(value, event);
  };

  handleOnBlur = (event) => {
    const { length, pattern } = this.props;
    const { value } = event.target;

    if (!_validateTextLength(length, value) || !_validatePattern(pattern, value)) {
      this.setState({ error: true });
      this.handleError(event, true);
      return true;
    }
    this.handleError(event, false);

    return null;
  };

  showLabel = (patternLabel, lengthLabel) => {
    if (patternLabel.length === 0 && lengthLabel.length === 0) return null;
    if (patternLabel.length === 0) return lengthLabel;
    if (lengthLabel.length === 0) return patternLabel;
    return `${patternLabel} (${lengthLabel})`;
  };

  render() {
    const {
      showLabels, lengthLabel, patternLabel, length, pattern, ...restProps
    } = this.props;
    const { error } = this.state;

    return (
      <>
        <TextField {...restProps} onBlur={this.handleOnBlur} onChange={this.handleChange} />
        {
          showLabels || error ? (
            <div
              className={classNames(styles.info, {
                [styles.error]: error,
              })}
            >
              {this.showLabel(patternLabel, lengthLabel)}
            </div>
          ) : null
        }
      </>
    );
  }
}

TextInput.propTypes = {
  onChange: PropTypes.func,
  showLabels: PropTypes.bool,
  length: PropTypes.array,
  lengthLabel: PropTypes.string,
  pattern: PropTypes.instanceOf(RegExp),
  patternLabel: PropTypes.string,
  onError: PropTypes.func,
};

TextInput.defaultProps = {
  showLabels: false,
  length: DEFAULT_LENGTH,
  lengthLabel: DEFAULT_LENGTH_LABEL,
  pattern: DEFAULT_TEXT_INPUT_PATTERN,
  patternLabel: DEFAULT_PATTERN_LABEL,
  onError: _noop,
  onChange: _noop,
};

export default TextInput;
