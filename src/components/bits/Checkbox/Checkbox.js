import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Checkbox from '@mui/material/Checkbox';

class AntCheckbox extends PureComponent {
  handleChange = (event) => {
    const { onChange } = this.props;
    onChange(event.target.checked, event);
  }

  render() {
    const { value } = this.props;
    return (
      <Checkbox
        {...this.props}
        checked={!value ? false : value}
        onChange={this.handleChange}
      />
    );
  }
}

AntCheckbox.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.any,
};

AntCheckbox.defaultProps = {
  value: false,
};

export default AntCheckbox;
