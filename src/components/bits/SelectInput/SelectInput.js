import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Select from '@mui/material/Select';

import { EMPTY_ARRAY } from 'constants/app.constants';

const { Option } = Select;

class SelectInput extends PureComponent {
  handleChange = (value) => {
    const { onChange } = this.props;
    onChange(value);
  }

  renderOption = (option, index) => {
    const { text, ...restProps } = option;
    return (
      <Option key={index} {...restProps}>{text}</Option>
    );
  }

  render() {
    const {
      options, ...restProps
    } = this.props;

    return (
      <Select {...restProps} style={{ width: '100%' }} onChange={this.handleChange}>
        {options.map(this.renderOption)}
      </Select>
    );
  }
}

SelectInput.propTypes = {
  onChange: PropTypes.func.isRequired,
  options: PropTypes.array,
};

SelectInput.defaultProps = {
  options: EMPTY_ARRAY,
};

export default SelectInput;
