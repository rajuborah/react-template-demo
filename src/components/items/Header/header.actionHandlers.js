import app from 'utils/axios';
import { _debounce } from 'utils/common';
import AUTH_CONTROL_ACTIONS from 'constants/redux-actions/authControl.actions';
import { EMPTY_ARRAY } from 'constants/app.constants';
import HEADER_ACTION_TYPES from './header.actionTypes';
const searchUser = _debounce(
  (setState, payload) => {
    const request = { url: `/api/events/${payload.eventId}/searchusers?searchText=${payload.value}`, method: 'GET' };
    if (payload.value !== '') {
      app.invokeApi(request, (success, res) => {
        if (success) {
          setState({ searchResult: res.data, isSearchLoading: false });
        } else {
          setState({ searchResult: EMPTY_ARRAY, isSearchLoading: false });
        }
      });
    } else {
      setState({ searchResult: EMPTY_ARRAY, isSearchLoading: false });
    }
  }, 2000
);

const toggleSearch = _debounce(
  (setState, payload) => {
    setState({ showSearchResult: !payload, searchText: '', searchResult: EMPTY_ARRAY });
  }, 500
);

const ACTION_HANDLERS = {

  [HEADER_ACTION_TYPES.TOGGLE_MENU]: async (_, { getState, setState }) => {
    const { isMenuActive } = getState();
    setState({ isMenuActive: !isMenuActive });
  },

  [HEADER_ACTION_TYPES.ONCHANGE_SEARCH_TEXT]: async ({ payload }, { setState }) => {
    setState({ searchText: payload.value, isSearchLoading: true });
    searchUser(setState, payload);
  },

  [HEADER_ACTION_TYPES.TOGGLE_SEARCH]: async ({ payload }, { setState }) => {
    toggleSearch(setState, payload);
  },

  [HEADER_ACTION_TYPES.LOGOUT]: async (_, { dispatch, getState }) => {
    const { history } = getState();
    const request = { url: '/oauth/logout', method: 'POST' };
    app.invokeApi(request, (success) => {
      if (success) {
        // notify.loading('You are logged out! Redirecting to login page...', 2, () => {
        //   history.push('/login');
        //   dispatch({ type: AUTH_CONTROL_ACTIONS.LOGOUT_SUCCESS });
        // });
        // GA.setCredential({ userId: 'GUEST' });
        // GA.event(gaCategories.USER, gaActions.SUCCESS, 'Logout');
      }
    });
  },
};

export default ACTION_HANDLERS;
