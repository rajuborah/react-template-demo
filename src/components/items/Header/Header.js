/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { PureComponent } from 'react';
import { Link, withRouter } from 'react-router-dom';

import PropTypes from 'prop-types';
import withActions from 'utils/connectors/withActions';
import { Spin } from 'antd';

import { EMPTY_STRING, EMPTY_ARRAY, EMPTY_OBJECT } from 'constants/app.constants';
import { _getHexCode } from 'utils/common';
import SynchronyLogo from 'assets/logo/insideChatLogo.png';
import SearchIcon from 'assets/icons/search.svg';
import { LoadingOutlined } from 'components/bits/Icons';

import { compose } from 'recompose';
import ACTION_HANLDERS from './header.actionHandlers';
import HEADER_ACTION_TYPES from './header.actionTypes';
import styles from './header.module.css';
// import GA from 'utils/ga';
// import { gaCategories, gaActions } from 'utils/ga/ga.constants';
const antIcon = <LoadingOutlined style={{ fontSize: 15, color: 'white' }} spin />;

class Header extends PureComponent {
  handleLogout = () => {
    const { onAction } = this.props;
    onAction({ type: HEADER_ACTION_TYPES.LOGOUT });
    onAction({ type: HEADER_ACTION_TYPES.TOGGLE_MENU });
  }

  handleMenu = () => {
    const { onAction, isMenuActive } = this.props;
    // GA.event(gaCategories.MISCELLANEOUS,  isMenuActive ? gaActions.CLOSE : gaActions.OPEN, 'Menu')
    onAction({ type: HEADER_ACTION_TYPES.TOGGLE_MENU });
  }

  renderMenu = () => {
    const { isMenuActive, match, userInfo } = this.props;
    const { eventId } = match.params;
    return (
      <>
        <div className={isMenuActive ? `${styles.menu} ${styles.menuActive}` : styles.menu}>
          <div className={styles.top}>MENU
            <button type="button" className={styles.closeBtn} onClick={this.handleMenu}>
              <div></div>
              <div></div>
            </button>
          </div>
          <ul className={styles.menuItems}>
            {(userInfo.userType !== 'Admin' && userInfo.userType !== 'SuperAdmin') && (
              <>
                {/* <li className={styles.menuItem} onClick={this.handleMenu}><Link to={{ pathname: `/${eventId}` }}>HOME</Link></li> */}
                <li className={styles.menuItem} onClick={this.handleMenu}><Link to={{ pathname: `/${eventId}`, hash: 'agenda' }}>AGENDA</Link></li>
                <li className={styles.menuItem} onClick={this.handleMenu}><Link to={{ pathname: `/${eventId}`, hash: 'experts' }}>EXPERTS</Link></li>
                <li className={styles.menuItem} onClick={this.handleMenu}><Link to={`/${eventId}/FAQ`}>FAQ</Link></li>
              </>
            )}
            <li className={styles.menuItem} onClick={this.handleMenu}><Link to={`/${eventId}/users/${userInfo.userId}`}>MY PROFILE</Link></li>
            <li className={styles.menuItem} onClick={this.handleLogout}>LOGOUT</li>
          </ul>
        </div>
        <div className={isMenuActive ? `${styles.backDrop} ${styles.backDropActive}` : styles.backDrop} onClick={this.handleMenu}></div>
      </>
    );
  };

 handleChange = (e) => {
   const { onAction, match } = this.props;
   const { eventId } = match.params;
   onAction({ type: HEADER_ACTION_TYPES.ONCHANGE_SEARCH_TEXT, payload: { eventId, value: e.target.value } });
 }

 toggleSearch = () => {
   const { onAction, showSearchResult } = this.props;
   onAction({ type: HEADER_ACTION_TYPES.TOGGLE_SEARCH, payload: showSearchResult });
 }

 render() {
   const {
     userInfo, searchText, searchResult, isSearchLoading, showSearchResult, location, match,
   } = this.props;
   const { eventId } = match.params;
   const { userType } = userInfo;
   
   if (location.pathname.endsWith('broadcast')) return null;
   return (
     <div className={styles.header}>
       <div className={styles.brand}>
         <Link to={userType && (userType === 'Admin' || userType === 'SuperAdmin') ? '/' : `/${eventId}`}><img src={SynchronyLogo} alt="Syncrony Logo" className={styles.logo} /></Link>
         {/* <div className={styles.event}>
           <img src={DiversityNetworks} alt="Diversity Networks Logo" className={styles.logo} />
         </div> */}
       </div>
       {userInfo.name && (
         <div className={styles.searchContainer}>
           <img src={SearchIcon} alt="search" className={styles.searchIcon} />
           <input type="text" placeholder="Search Attendees" value={searchText} onChange={this.handleChange} onFocus={this.toggleSearch} onBlur={this.toggleSearch} />
           {isSearchLoading && (<Spin indicator={antIcon} className={styles.spiner} />)}
           <div className={showSearchResult ? styles.searchResults : styles.searchResultsHide}>
             {searchResult.map(result => (
               <Link to={`/${eventId}/users/${result.id}`} key={result.id} className={styles.result}>
                 <div style={{ backgroundColor: _getHexCode(result.network) }} className={styles.dot}></div>
                 {result.first_name ? `${result.first_name} ${result.last_name}` : `${result.username}`}
               </Link>
             ))}
           </div>
         </div>
       )}
       <div className={styles.rightSide}>
         {userInfo.name ? <div className={styles.welcomeText}>WELCOME <b>{userInfo.name}</b></div> : ''}
         {userInfo.name && (
           <button type="button" className={styles.burgerBtn} onClick={this.handleMenu}>
             <div></div>
             <div></div>
             <div></div>
           </button>
         )}
       </div>
       {this.renderMenu()}
     </div>
   );
 }
}

Header.propTypes = {
  onAction: PropTypes.func.isRequired,
  userInfo: PropTypes.object.isRequired,
  eventId: PropTypes.string,
  isMenuActive: PropTypes.bool,
  searchText: PropTypes.string,
  searchResult: PropTypes.array,
  isSearchLoading: PropTypes.bool,
  showSearchResult: PropTypes.bool,
  location: PropTypes.object,
  match: PropTypes.shape({
    params: PropTypes.shape({
      eventId: PropTypes.string,
    }),
  }).isRequired,
};

Header.defaultProps = {
  eventId: EMPTY_STRING,
  isMenuActive: false,
  searchText: EMPTY_STRING,
  searchResult: EMPTY_ARRAY,
  isSearchLoading: false,
  showSearchResult: false,
  location: EMPTY_OBJECT,
};

export default compose(
  withRouter,
  withActions(ACTION_HANLDERS),
)(Header);
