import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Button from 'components/bits/Button';
import { _noop, _isString } from 'utils/common';
import { EMPTY_OBJECT, EMPTY_ARRAY, EMPTY_STRING } from 'constants/app.constants';
import FORM_ACTION_TYPES from './formBuilder.actionTypes';
import styles from './formBuilder.module.css';

class FormBuilder extends PureComponent {
  componentDidMount() {
    const { onAction } = this.props;
    onAction({ type: FORM_ACTION_TYPES.ON_FORM_INIT });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { onAction } = this.props;
    onAction({ type: FORM_ACTION_TYPES.ON_FORM_SUBMIT });
  }

  renderColumn = (key, index) => {
    if (!_isString(key)) return this.renderSection(key, index);
    const { fields, values, onAction } = this.props;
    if (!fields[key]) throw new Error(`Config missing for ${key}`);
    const { renderer: Component, renderOptions } = fields[key];
    const value = values[key];
    return (
      <React.Fragment key={key}>
        <Component onAction={onAction} id={key} {...renderOptions} value={value} />
      </React.Fragment>
    );
  }

  renderRow = ({ className, columns }, index) => (
    <div key={index} className={cx(styles.rowContainer, className)}>
      {columns.map(this.renderColumn)}
    </div>
  )

  renderSection = ({ rows, className }, index) => (
    <div key={index} className={cx(styles.sectionContainer, className)}>
      {rows.map(this.renderRow)}
    </div>
  )

  renderForm = () => {
    const { sections } = this.props;
    return sections.map(this.renderSection);
  }

  renderSubmit = () => {
    const { submitButtonText, isSubmitLoading, showSubmit } = this.props;
    return showSubmit ? (
      // loading={isSubmitLoading}
      <Button type="primary" onClick={this.handleSubmit}>{submitButtonText}</Button>
    ) : null;
  }

  render() {
    const { className, style } = this.props;

    return (
      <form className={cx(styles.formContainer, className)} style={style}>
        {this.renderForm()}
        {this.renderSubmit()}
      </form>
    );
  }
}

FormBuilder.propTypes = {
  sections: PropTypes.array,
  fields: PropTypes.object,
  values: PropTypes.object,
  onAction: PropTypes.func,
  submitButtonText: PropTypes.string,
  isSubmitLoading: PropTypes.bool,
  className: PropTypes.string,
  showSubmit: PropTypes.bool,
  style: PropTypes.object,
};

FormBuilder.defaultProps = {
  sections: EMPTY_ARRAY,
  fields: EMPTY_OBJECT,
  values: EMPTY_OBJECT,
  style: EMPTY_OBJECT,
  onAction: _noop,
  submitButtonText: 'Submit',
  isSubmitLoading: false,
  className: EMPTY_STRING,
  showSubmit: true,
};

export default FormBuilder;
