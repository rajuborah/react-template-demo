export const defaultOnChangeHandler = ({ payload: { key, value } }, { getState, setState }) => {
  const { values } = getState();
  setState({ values: { ...values, [key]: value } });
};

export const defaultOnErrorHandler = ({ payload: { key, value } }, { getState, setState }) => {
  const { values } = getState();
  setState({ values: { ...values, [key+'Error']: value } });
};
